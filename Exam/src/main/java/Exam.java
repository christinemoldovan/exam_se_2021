import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/* EXERCISE 1

public class I implements U{
    private long t;
    private K k; //association I has a K

    public void f(){}
    public void i(J j){}//dependency I refers to B
}
public class J{}
public class N{
    private I i; //association N has an I
}
public class K{
    private L l;//composition
    private S s;//aggregation

    public void setL(){
    this.l=new L(); //class L created inside K only
    }

    public void setS(S s_ref){
    this.s=s_ref; //class S created outside and passed to K
    }
}
public class L{
    public void metA(){}
}
public class S{
    public void metB(){}
}
interface U{}

*/


//EXERCISE 2
public class Exam extends JFrame {
    private JTextField textField1 = new JTextField();
    private JTextField textField2 = new JTextField();
    private JButton button1 = new JButton();
    private JPanel contentPane = new JPanel();

    boolean check = false;//to check if it is in a text-field

    public Exam() {
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        contentPane.add(textField1, BorderLayout.NORTH);
        contentPane.add(textField2, BorderLayout.SOUTH);

        button1.setPreferredSize(new Dimension(40, 40));
        contentPane.add(button1, BorderLayout.CENTER);

        pack();

        textField1.setText("");
        textField2.setText("");
        textField1.setEditable(false);
        textField2.setEditable(false);

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String aux = "This is a test"; //predefined input
                if (check == false) {
                    textField1.setText(aux);
                    textField2.setText("");
                    check = true;
                } else {
                    textField1.setText("");
                    textField2.setText(aux);
                    check = false;
                }
            }
        });
    }

    public static void main(String[] args) {
        Exam frame = new Exam();
        frame.setTitle("Move text from text-fields");
        frame.setSize(300, 100);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}

